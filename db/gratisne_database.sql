-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 12, 2022 at 09:03 PM
-- Server version: 10.3.35-MariaDB-cll-lve
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gratisne_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `category_id`, `width`, `height`, `slug`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Otto kortingen en meer! Tot 40%!', 2, 250, 115, 'Otto-kortingen-en-meer!-Tot-40!', '20210924180900.png', 'Krijg bij Otto.nl korting tot aan 50~80%! Otto is een welbekend kleding,accessoires en apparatuur website die veel verder gaat dan het gewone. Haal alles in huis van bed tot aan fornuis tot aan kleding en evenzo apparaten als (boormachines, tandenborstels en verlengsnoeren) alles te vinden bij Otto.NL<br><br>Otto bied daarom ook vaak gave kortingen die voor iedereen beschikbaar zijn! Wist je dat je bij Otto door het jarig te zijn korting krijgt van 15%+ korting? Dit alles en meer!<br><br>Weleens zijn er acties voorbij gekomen waarbij je geen 21% btw hoeft te betalen in alle assortimenten zoals al opgenoemd van apparatuur tot aan smartphones! Daarom bezoek Otto.nl vandaag nog en maak kans op te gekke kortingen.<br><br><p>		  <a href=\"#\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></p><p>Ongeacht de corona maatregelen kun je als nog lekker genieten bij Otto.nl! Volgens Otto is er geen verschil tussen voor en na corona er word alsnog volop geshopt! Wat wel betekend dat er extra drukte is bij klantenservice, vervoerders en of magazijn.</p><p>Hoe dan ook Otto stelt zich nog steeds open en flexibel voor veranderen! Wel is het zo dat vanwege de hoeveelheid bestellingen die nu verplicht online moeten worden gedaan het iets langer kan duren voordat het pakket bij jou thuis is afgeleverd.</p><p> Ze doen hun uiterste best om het pakket zo snel mogelijk te versturen en ervoor te zorgen dat het zo snel mogelijk bij jou thuis wordt afgeleverd. (wat ze natuurlijk niet erbij zeggen is dat garantie niet beloofd is - maar ze doen hun best )Ze doen hun best om elke verspreiding van het virus tegen te gaan!</p><p>Ook is er een betalingstermijn van 30 dagen bij Otto.nl!</p><p>Zoals staat vermeld op hun website: \"<span style=\"font-size: small;\"><br>Je \r\nhebt 30 dagen de tijd om je artikelen te retourneren maar we vragen je \r\nwel om binnen 14 dagen na ontvangst van je artikel(en) te betalen. Mocht\r\n je na de betaling toch nog besluiten om de artikelen te retourneren, \r\ndan storten we het naar OTTO overgemaakte bedrag terug. Mocht je jouw \r\nbestelling willen retourneren, maar ben je niet in de mogelijkheid om te\r\n retourneren binnen de zichttermijn van 30 dagen? Neem dan direct \r\ncontact op met onze klantenservice</span>\"</p><p>Dat is altijd mooi om te weten!</p><p><br>Informatie Otto:<br><br>Het assortiment omvat mode-, woon-, multimedia- en huishoudelijke producten.<br><br>Producten: mode, wonen, tuin, klussen, elektronica, computer en sport. <br></p>', '2021-02-02 12:57:35', '2021-09-24 16:09:00'),
(3, 'Wehkamp kortingscode €6 euro korting', 3, 250, 180, 'Wehkamp-kortingscode-€6-euro-korting', '20210323140311.jpg', 'Wehkamp is een Nederlands webshop hun imago is om te zetten via deze termen <span>\"postorderbedrijf en online warenhuis\". Simpel gezegd een webshop waar je bijna alles kunt krijgen. Ze staan eronder bekend dat ze van alles in huis hebben; koelkasten, tv\'s, spel-computer tot aan laptops etc.</span><p><span><br></span></p><h3>Je kan werkelijk alles vinden!</h3><p><span>Je kan er werkelijk alles in terug vinden. Kleding zijn evenzo zaken die te vinden zijn op Wehkamp. Daarom raden wij Wehkamp aan als zijner de plaats waar je kunt shoppen voordelig! Wil je zien en weten wat Wehkamp allemaal te bieden heeft? Bekijk hem hier onder wat Wehkamp voor jou heeft klaarliggen. Bij Wehkamp zijn diverse kortingen beschikbaar in verschillende categorieën het komt weleens voor dat Wehkamp fikse kortingen geeft zoals 30-50% korting. <br></span></p><p><span><br></span></p><h3><span>Diverse kortingen </span></h3><p><span>Ontdek wat Wehkamp dit keer heeft qua korting! Zodra wij een Wehkamp korting hebben updaten wij dit artikel zodat alle gebruikers gebruik kunnen maken van deze coupon. Maak snel gebruik van de link hier beneden voordat de kortingen aflopen! Bestel vandaag nog bij Wehkamp</span></p><p><span><br></span><br><span><a href=\"#\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></span></p><p></p><h3><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span>Informatie Wehkamp:</span></font></span></h3><p><span>Wehkamp is een onderdeel van Apax Partners. Het online warenhuis telt \r\nongeveer 2,7 miljoen vaste klanten en verzendt zo\'n 11 miljoen pakketten\r\n per jaar. Het assortiment bestaat uit ruim 400.000 verschillende \r\nartikelen van zo’n 2.000 merken, voornamelijk in de categorieën mode, \r\nwonen, beauty en baby/kind Bron:Wikipedia.</span></p>', '2021-02-07 14:46:19', '2021-03-23 13:03:11'),
(4, 'HEMA Zorgverzekering!', 1, 250, 200, 'HEMA-Zorgverzekering!', '20210211140656.png', '<p>Het hebben van een verzekering is in Nederland verplicht en vereisend in geval van ongevallen en noden. Zorgverzekeringen kunnen een geweldige manier zijn om heel veel schulden te voorkomen. </p><p>Vooral als je niet bij een zorgverzekering ben verzekerd die jou volledig dekt. Gelukkig kennen wij een zorgverzekering die zich ideaal aan jou aansluit! Dan praten wij over HEMA Zorgverzekering!</p><p><br></p><h2><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\">HEMA Zorgverzekering</span></font></font></span></h2><h2></h2><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\">,,...<i>Pardon? HEMA verzekering?</i> <i>Ik dacht dat HEMA een keten was waar je voornamelijk kleren kon kopen, accessoires en eventueel een ontbijt scoren?\" </i></span></font></span></font></font></span></h6><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\">Daarom maak je zeker een punt! HEMA is ook begonnen met een eigen zorgverzekering. Een zorgverzekering als het Zilveren Kruis, VGZ en nog meer. Maar dit keer de HEMA Zorgverzekering.</span></font></span></font></font></span></h6><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\"><a href=\"#\" class=\"knop\" target=\"_blank\">Ontvang deze actie!</a><br></span></font></span></font></font></span></h6><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\">HEMA weet wat de mens nodig heeft en vandaar dat zij begonnen zijn met een eigen zorgverzekering. Met de HEMA Zorgverzekering krijg je ook nog is 10% korting in de winkel van HEMA zelf. Idealer dan dat kan het niet worden. Als HEMA jou favoriete winkel is dan mag je in dit zeker niet mislopen! De voordelen die eraan verbonden zijn onlosmakelijk aantrekkelijk. Voordelen die je zeker niet kan missen. </span></font></span></font></font></span></h6><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\"><br></span></font></span></font></font></span></h6><h6><span style=\"background-color: rgb(255, 0, 0);\"><font color=\"#000000\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 255);\">1. G</span></font></span></font></font></span>ewoon naar ieder ziekenhuis en huisarts in NL\r\n        </h6><p>2. Ze zijn altijd online en telefonisch beschikbaar.</p><p>3. Ze bieden jou mij een helder inzicht wat wel en niet vergoed is.</p><p><br></p><p>Dit zijn voornamelijk de opvallende voordelen van de HEMA. Slechts te beginnen bij €119,- eventueel zonder eigen risico is het €95,- Indien je graag naar de fysio wil is dit ook inbegrepen in jouw pakket. Daarom maak snel gelegenheid van deze actie en deal!</p><p><br></p><h3 class=\"title--s\">,,<i>de juiste zorg voor iedereen, sinds 2014</i>\" -HEMA<br></h3><p> </p> <br>', '2021-02-11 13:00:51', '2021-02-11 13:06:56'),
(9, 'Waarom je pizza hier zou moeten bestellen!', 1, 250, 200, 'Waarom-je-pizza-hier-zou-moeten-bestellen!', '20210301174403.png', '<p>Er zijn tegenwoordig zoveel restaurants waar je pizza kan bestellen. Pizza\'s zijn tegenwoordig heel trending iedereen heeft er af en toe wel zin in. Als je bijvoorbeeld net thuis komt van werk en je hebt geen zin om te koken dan is het bestellen van pizza een prima alternatief. Maar nu komt de grote en ingewikkelde vraag: waar zal ik het bestellen? Want zoals boven vermeld. Er zijn er zoveel!!</p><h3>Waar moet ik bestellen?!?!?</h3><p>We kennen verschillende pizza-ketens als Dominos, New Yorker Pizza en nog meer verschillende pizza ketens die rondom het internet krioelen met hun gratis deals en aanlokkelijke acties. Maar het gaat uiteindelijk om de sensatie en de service die vooral bij mensen blijft hangen. Evenzo wordt er ook gekeken of de pizza smakelijk is of de smaakpillen ervan op til gaan slaan.</p><h3>Verschillende pizza-ketens..<br></h3><p>Wij kwamen verschillende pizza ketens tegen en probeerden ze maar deze sprong er toch wel echt uit: Papa Johns. Wellicht ken je het wel of misschien niet maar zij biedt kwalitatieve pizza\'s aan voor een aanzienlijke prijs. Ze komen tenslotte oorspronkelijk uit Amerika en wij weten waar de pizza het meest geliefd is en waar pizza\'s op hele hoge kwaliteit worden geleverd dan spreken we toch over Amerika!</p><h3>Positieve reviews<br></h3><p>Papa Johns heeft meer dan 13.000 positieve reviews over hun pizza dit zegt niet zomaar wat; maar maakt duidelijk openbaar dat dit een plaats is die je niet zo snel zal vergeten. Ook waren beginnende ondernemers vrezend toen zij zagen dat Papa Johns ook evenzo naar Nederland kwam; het was voor hun een nachtmerrie! Maar nu even terug: het bestellen van pizza!<br></p><h3>Prijs ,,tussen de €9,99 - €12,99\"</h3><p>De prijzen spelen ook een groot rol onder pizza-ketens maar voor maar €13,- heb je al een lekkere \"Chicken Popper Combo\" voor de boeg. De prijzen variëren ontzettend. De prijzen liggen ongeveer tussen de €9,99 - €12,99.Je kunt hier makkelijk pizza bestellen via de website. Ze doen zowel aan ophalen als aan leveren. Zij voeren het corona beleid uitvoerig uit om jou een veilige pizza aan te bieden. <br></p><p><br></p><h3>2 halen 1 betalen!</h3><a href=\"https://fr135.net/c/?si=15336&amp;li=1672038&amp;wi=359163&amp;ws=\" rel=\"sponsored\" target=\"_blank\"><img src=\"https://animated.dt71.net/15336/1672038/?wi=359163&amp;ws=\" alt=\"\" style=\"max-width:100%; height:auto; border:none;\"></a>', '2021-02-15 20:31:16', '2021-03-01 16:44:03'),
(10, 'Ebike accu\'s waar te halen', 1, 250, 120, 'Ebike-accu\'s-waar-te-halen', '20210218203813.png', '<p></p><p>Beschik jij toevallig over een E-bike en is toevallig jouw accu stuk gegaan of heb je een E-bike fiets gekocht zonder accu en wil je er één aanschaffen? Waar kun je voordelig en kwalitatieve accu\'s kopen? Aangezien het internet er vol van is. Wat is dé website om productieve E-bike accu\'s te kopen? Wij kennen er meerdere zoals u ook meerdere kent, maar wij kozen voor één voor u.<br></p><h5>,,E-bikeaccuspecialist\"</h5><h2>Het hebben van de juiste accu</h2>Heel veel mensen weten dat een accu kopen voor een prijs die te mooi om waar te zijn meestal ook inderdaad is om waar te zijn. Ze komen erachter dat zij die voor heel laag worden verkocht meestal ook de accu\'s zijn die het snelst defect raken. Hoewel het duidelijk is zijn er wel uitzonderingen. Wij proberen absoluut niet te verkopen dat E-bikeaccuspecialist de meest goedkoopste noch spreken wij tegen dat het meest dure is wat er te vinden is. Maar wij spreken dat het voor zijn kwaliteit het waard is.<br>Waarom deze?<p></p><p>Zij hebben een ruime assortiment van accu\'s beschikbaar en niet alleen van accu\'s maar ook van accessoires of zelfs accu opladers. Ze bieden de klant verschillende categorieën van dure accu\'s die veel langer meegaan tot aan goedkope accu\'s die voor kleine afstanden zijn. Ze bieden jou de optie: ,,welke wil je? Wij hebben ze!\" Dit is natuurlijk ook zeer aangenaam voor klanten die opzoek zijn naar accu\'s.<br><u>,,Wat jou budget ook is zij hebben ze!\"</u><br></p><h2><a href=\"https://tidd.ly/3pvn61d\" class=\"knop\" target=\"_blank\">Neem deel aan deze deal!</a></h2><h2>Wat is de prijs?</h2>De prijs ligt hem tussen de €249,- tot aan €600,- Het hangt helemaal van jou af! Wat wij wel aanraden is natuurlijk om te investeren in iets wat natuurlijk wel langer meegaat. De kwaliteit is beter dan de kwantiteit. Voor €250 heb je al een goedkope accu voor de boeg en dan spreken we niet over geen kwalitatieve accu\'s maar voordelige accu\'s met een goed prijskaartje.<p></p><h2>E-bikeaccuspecialist</h2>Wij raden E-bikeaccuspecialist aan als zijner de beste plek om accu\'s te bestellen. Indien er vragen zijn kun je altijd contact opnemen met de klantenservice van E-bikeaccuspecialist. Voor zowel een goedkoop prijskaartje als een vrijwel dure prijskaartje kun je een goede accu bezitten. Wacht niet langer bezoek de website nu!<p></p>', '2021-02-18 13:03:33', '2021-02-18 19:38:13'),
(11, 'Waar kun je het best kleding kopen', 1, 250, 200, 'Waar-kun-je-het-best-kleding-kopen', '20210302161400.png', '<p>Er zijn verschillende locaties (geografisch gezien) als websites waar je voordelig kan shoppen voor kleren? Maar welke is echt een uitblinker? Velen zijn sommige warenketens zat waar de klantenservice niet zijn zoals ze moeten zijn. Uit onderzoek blijkt dat klantvriendelijk heel erg bepalend is tot terugkerend verkeer. Wanneer mensen blij zijn met hun aanschaf en ook nog is de klantservice op en top is dan blijft dat een echte indruk maker! </p><h2>Niet vermoeien...<br></h2><p>Wij willen jou natuurlijk vermoeien met een lap tekst maar wij willen wel even goed alles neerzetten. Waardoor jij weet wat jou te wachten staat. Wij staan achter Avanti Sport. Avanti sport is een keten die diverse producten aanbied van schoenen tot aan kleding tot aan beroemde merken. Alles is te verkrijgen bij Avanti Sport.</p><h2>Favoriete merk?</h2><p>Dat kan absoluut bij Avanti Sport. Je kan van adidas tot aan nike bij hun vinden. Niks is voor hun niet te verkrijgen. Het assortiment van Avanti Sport is breed ze hebben evenzo ook Levi\'s die te verkrijgen zijn. </p><h2>Duur?</h2><p>Qua prijs valt het reuzen mee! Voor maar €54,95 weet je al een mooie schoen voor elkaar te krijgen. Prijs is zeker \"not the issue\". Wel kan het zijn dat ze niet alle soorten merken hebben. Wat natuurlijk kan liggen omdat het uitverkocht is of omdat het niet zeer geliefd is onder de mensen. Hangt van alles af. </p><h2>Ontdek wat zij hebben!</h2><p>Wacht niet langer kijk wat ze vandaag te bieden hebben!</p><p><a href=\"https://tidd.ly/3qf1uXv\" class=\"knop\" target=\"_blank\">Bezoek Avanti Sport!</a><br></p>', '2021-03-01 18:42:21', '2021-03-02 15:14:01'),
(12, 'Adidas vs Puma', 12, 250, 200, 'Adidas-vs-Puma', '20210303162209.png', '<p>Welke gaat winnen? Wat is nou echt beter? En misschien ben jij wel een voetballiefhebber welke bekende atleten maken gebruik van de bekende merken Puma en Adidas? Puma is opgericht door de persoon <span>Rudolf Dassler in 1924. Exact 1924!</span></p><p><span>Sindsdien is Puma zeer bekend onder de jongeren en (ook wel) ouderen. Sommige erkennen dat zij heel erg lekker lopen. Anderen geven te kennen dat het een imposante logo heeft, aanlokkelijk voor het oog. Puma faciliteert zich op allerlei categorieën bijvoorbeeld met </span><span><span>onderbroeken, sokken en laarzen,</span> die niet zo snel op de markt te vinden zijn. <br></span></p><h2><span>Wat Puma aanlokkelijk maakt voor jongeren</span></h2><p><span>Voetbalatleet Neymar Jr maakt gebruikt van de merk Puma. Terwijl Messi gebruikt maakt van Adidas zijn heel veel mensen toch wel onder de indruk hoe Neymar bij PSG (huidige team) zoveel indruk weet te maken en doelpunten weet te scoren waardoor hij toch wel een opvallertje weet te worden onder de jongeren met de kicksen Puma!</span></p><h2><span>Puma blijft uniek!</span></h2><h6><span>Wat je ook maar wil zeggen van Puma.. het blijft uniek! Zij zullen nooit compromissen sluiten om hun fans teleur te stellen. Puma is namelijk een duitse onderneming die sportartikelen produceert. Daarnaast is Puma niet duur! Te midden van een corona tijdperk weet Puma zijn prijzen toch wel betaalbaar te houden waardoor de nieuwe collecties toch altijd haalbaar blijft tot aanschaf.</span></h6><h6><span><br></span></h6><h2><span>Wil je Puma uitproberen?</span></h2><h5><span>GratisNetwerk heeft een link gedeeld met onze gebruikers die Puma uit willen proberen; zodra wij kortingen weten te krijgen zullen wij die meteen naar jullie doorspelen. Hou onze website goed in de gaten. \r\n<script type=\"text/javascript\">\r\nvar uri = \'https://impnl.tradedoubler.com/imp?type(img)g(24113330)a(3209707)\' + new String (Math.random()).substring (2, 11);\r\ndocument.write(\'<a href=\"https://clk.tradedoubler.com/click?p=284496&a=3209707&g=24113330\" target=\"_BLANK\"><img src=\"\'+uri+\'\" border=0></a>\');\r\n</script><br></span></h5>', '2021-03-03 15:14:50', '2021-03-03 15:22:09'),
(13, 'Nieuwe warenketen gelijk Wehkamp!', 3, 250, 200, 'Nieuwe-warenketen-gelijk-Wehkamp!', '20210304125823.png', '<p>Heb je weleens gehoord over Wehkamp? Vast wel hé! Wehkamp is een van de grootste warenhuizen die er maar te vinden is in Europa. Maar wacht Bonprix zeg je? Wat is dat? Nou  Bonprix is een net zo grote warenhuis als Wehkamp die niet alleen zich op één front gebied manifisteert maar evenzo internationaal!</p><h2>Hoe groot liggen de verschillen?</h2><p>Moeilijk uit te leggen.. Aangezien wij niet de exacte cijfers hebben die Wehkamp maandelijks binnenkrijgt en Bonprix. Maar qua prijs is er wel een immens groot verschil. Bonprix heeft producten klaarliggen wat je daaronder kan verstaan zijn sweaters, shirts, broeken en sokken enzovoorts voor aanzienlijk goedkope prijs! Het is bijna onbedenkelijk!</p><h2>Kwantiteit of Kwaliteit?<br></h2><p>Wel moeten wij zeggen dat Wehkamp producten verkoopt die zeer van grote kwaliteit zijn! Maar Bonprix komt heel dicht in de buurt of kan bijna wel zeggen is in de buurt! Aangezien zij zulke voordelige prijzen hebben die gepaard gaan met kwaliteit is het wel bijna te zeggen: zij leveren wat de klant wilt!</p><h2>Prijs?</h2><p>Qua prijs kunnen wij alleen adviseren zelf een oogje te nemen op de website van Bonprix als die van Wehkamp! Je zult een verschil merken en zien.<b> Voor €8,- heb je al een shirt bij Bonprix gescoort!</b></p><h2>Gratis levering!</h2><p>Elke nieuwe klant heeft&nbsp; recht tot een gratis levering die kan lopen tot aan €5,-&nbsp; Dat zijn kansen die je absoluut niet moet missen! Op dit moment zijn er ook <u><b>wintersales</b></u>! <br></p><p><br></p><script type=\"text/javascript\">\r\nvar uri = \'https://impnl.tradedoubler.com/imp?type(img)g(24919698)a(3209707)\' + new String (Math.random()).substring (2, 11);\r\ndocument.write(\'<a href=\"https://clk.tradedoubler.com/click?p=22449&a=3209707&g=24919698\" target=\"_BLANK\"><img src=\"\'+uri+\'\" border=0></a>\');\r\n</script><p><br></p><p><br></p>', '2021-03-04 11:52:05', '2021-03-04 11:58:23'),
(14, 'Nike halen Dat kan hier! Tot 25% korting', 10, 250, 150, 'Nike-halen-Dat-kan-hier!-Tot-25-korting', '20210924180733.png', '<p>Ben je van plan Nikes te gaan kopen? En weet je niet waar je ze voor een voordelige prijs kan halen? Dan hebben wij één voor jou gevonden die super voordelig is en super makkelijk te navigeren en dat is de website: Aktiesport. </p><h2>Nikes duur? Hier niet.</h2><p>Het kopen van Nikes zijn tegenwoordig in! In welke doelgroep jij je ook maar bevind hij is staat heel erg hip. Maar vaak zijn de Nikes die het mooist zijn ook prijzig en niet goedkoop. Vaak zit je al snel op de €150-200 voor een goed en ook nog is leuk model van Nike. En dan niet gesproken over trainingspakken! Maar bij Aktiesport is hij vrijwel goedkoper dan de rest!<br></p><h2>Betaalbaar prijs</h2><p>Voor een betaalbaar prijs weet je al gauw een mooie Nike schoen voor elkaar te krijgen. Ook zijn er heel veel variaties; dus keuze zat! Ook bieden ze jou omdat jij je het misschien niet zo makkelijk ziet zitten qua financieel achteraf betalen via Klarna! Dat stel je zou hem wel aan het doen en passen en uitlopen kan je altijd na 14 dagen je betaling verrichten!</p><h2>Oké.. Maar wat zijn de prijzen?</h2><p>De prijzen variëren enorm maar om maar een begin som te maken voor; €28,13 t/m 129,- weet je al een mooie schoen te kopen! <u><i>Pardon! €28,13? Is dat mogelijk?&nbsp; </i></u>Kennelijk wel bij Aktiesport! Wel moeten we even erbij melden dat dit een actie is die tijdelijk. Maar grijp je kan voordat het voorbij is!</p><h2>Retourrecht</h2><p>Ze bieden jou evenzo ook 60 dagen retourrecht en vanaf €29,99 is verzendkosten gratis! Doen, doen doen! Grijp je kans! Hou deze pagina goed in de gaten wij zullen eventueel kortingscodes toevoegen!</p><a href=\"https://tc.tradetracker.net/?c=389&amp;m=1108349&amp;a=398285&amp;r=&amp;u=\" target=\"_blank\" rel=\"sponsored nofollow\"><img src=\"https://ti.tradetracker.net/?c=389&amp;m=1108349&amp;a=398285&amp;r=&amp;t=html\" alt=\"sneakers\" width=\"728\" height=\"90\" border=\"0\"></a><p><br></p>', '2021-03-05 17:40:04', '2021-09-24 16:07:33'),
(16, 'Tot 70% korting nike voetbalschoenen', 10, 300, 120, 'Tot-70-korting-nike-voetbalschoenen', '20210924180719.jpg', '<p>Nike voetbalschoenen zijn nu met korting verkrijgbaar bij PlayFootbal.Shop. Wellicht ken je de website niet dan maken wij het even duidelijk voor jou. Het staat vol van allerlei assortimenten van diverse merken zoals Adidas, Nike en Puma. Alles is er letterlijk te vinden!</p><h2>Goedkoop + korting!</h2><p>Bij PlayFootbal.Shop kun je voor een voordelige prijs inclusief korting een mooie Nike voetbalschoen scoren. Mooier dan dat ga je niet vinden. Wil je een voetbalpak, voetbalschoen of voetbalsokken? Bij Playfootbal.shop zijn ze allemaal te vinden voor een voordelige prijs.</p><h2>Voor 16:00 besteld morgen thuis!</h2><p>Het staat ook zo dat wanneer je voor 16:00 besteld dat je hem de volgende morgen al heb. Dat is nog mooier meegenomen dan hoef je niet te lang te wachten om gebruik te maken van je nieuwe voetbalschoenen. Maak gebruik van deze actie nu het nog kan.</p><h2>Gratis verzending vanaf €50,-</h2><p>Door een voetbalschoen te kopen (die boven de €50,- zit) ben je verzekerd voor het niet hoeven te betalen van de verzendkosten. Idealer dan dit ga je niet vinden!</p><h2>Ga naar actie</h2><a href=\"https://www.playfootball.shop/voet/?tt=31477_1718450_398285_&amp;r=\" target=\"_blank\" rel=\"sponsored nofollow\"><img src=\"https://ti.tradetracker.net/?c=31477&amp;m=1718450&amp;a=398285&amp;r=&amp;t=html\" alt=\"Playfootball Super Sale\" width=\"728\" height=\"90\" border=\"0\"></a>', '2021-03-17 12:51:52', '2021-09-24 16:07:19'),
(17, 'Pak deze geweldige sales van Nike 30% korting', 10, 360, 200, 'Pak-deze-geweldige-sales-van-Nike-30-korting', '20210924180707.png', '<p>eweldige sales om niet uit het oog te verliezen. Iedereen houdt van sales (in het Nederlands: uitverkoop). We zijn er dol op en het geeft ons gevoel van kostbewust. Omdat wij alsnog onze verlangens kunnen waarmaken door leuke koopjes voor onszelf aan te schaffen maar daarnaast ook fikse korting erbij te krijgen! Sporthuis is er één van!</p><h2>Korting tot 30% voor nike</h2><p>Ben je opzoek naar fijne kortingen zoals 20%, 30% of misschien wel 50%? Dan ken je Sporthuisnog niet. Het is ideaal voor mensen die opzoek zijn naar een Nike product maar niet zoveel willen betalen. Ze bieden een ruime assortiment op het gebied qua Nike en meer. Misschien ben je Adidas liefhebber ook die hebben zij in de voorraad. </p><h2>Regelmatig kortingscodes</h2><p>Zij bieden regelmatig ook kortingscodes aan die beschikbaar worden gemaakt voor klanten om het nog goedkoper te maken dan het eigenlijk al is. Nog meer reden om Sporthuis een kans te geven. </p><h2>Eigen winkel</h2><p>Ook hebben zij een fysieke winkel waar je naar toe kan gaan dat maakt het voor klanten nog betrouwbaar aangezien zij indien er problemen zijn of indien zij het willen ophalen zelf naar daar kunnen gaan om het op te halen of het probleem op te lossen.</p><h3><span style=\"background-color: rgb(255, 255, 0);\"><font color=\"#000000\">Bezoek actie </font></span></h3><a href=\"https://www.sporthuis.nl/assortiment/?tt=4689_1310413_398285_&amp;r=\" target=\"_blank\" rel=\"sponsored nofollow\"><img src=\"https://ti.tradetracker.net/?c=4689&amp;m=1310413&amp;a=398285&amp;r=&amp;t=html\" alt=\"\" width=\"728\" height=\"90\" border=\"0\"></a>\r\n<a href=\"https://www.sporthuis.nl/assortiment/?tt=4689_12_398285_https%3Awww.sporthuis.nlrunninghardloopschoenenlnike&amp;r=%2F\" target=\"_blank\" rel=\"sponsored nofollow\"></a>', '2021-03-19 12:45:20', '2021-09-24 16:07:07'),
(18, 'Goedkope Nike Trainingspakken 30% korting', 10, 480, 180, 'Goedkope-Nike-Trainingspakken-30-korting', '20210924180632.png', '<p>Heb je altijd goedkope Nike trainingspakken gewild? Dit is dan jouw kans! Er is een vette korting die het mogelijk maakt dat jij een mooie en kwalitatieve trainingspak van Nike kan scoren! Deze korting zal waarschijnlijk niet lang zal gelden. Daarom is het des te meer nu de tijd op deze actie te grijpen</p><h2>Soccerfanshop</h2><p>Deze korting is te vinden op de Soccerfanshop zelf hebben wij nooit eerder van die naam of webshop gehoord maar ze bieden wel geweldige deals aan die het mogelijk maken dat je een geweldige Nike trainingspak kan scoren voor een goedkope prijs! Zij bieden niet alleen trainingspakken aan maar ook gewoon normale kleding van allerlei soorten merk! <b>Adidas, Puma en Nike.</b></p><h2><span style=\"font-weight: normal;\">Sales pagina</span></h2><h2><span style=\"font-weight: normal;\"></span></h2><h6>Zij hebben niet alleen te gekke kortingen zoals boven vermeld op Nike maar ook hebben zij verschillende merken waar sales op te halen is! Zij hebben zelfs hun eigen Sales pagina ingericht met allerlei dure merken maar die toch voor een voordelige prijs te koop zijn! Dat is nou top!</h6><h2>Verzend informatie?</h2><p>Heb je een product besteld kan van alles zijn dan gelden deze regels: </p><p>Bestellingen voor 23:59 (ma t/m do) besteld worden de volgende dag geleverd.</p>\r\n<p>Bestelling op vrijdag voor 20:30 besteld worden op zaterdag geleverd.</p><h3><span style=\"background-color: rgb(255, 255, 0);\"><font color=\"#000000\">Bezoek actie:</font></span></h3>\r\n<a href=\"https://tc.tradetracker.net/?c=9750&amp;m=12&amp;a=398285&amp;u=%2Fproduct-categorie%2Fvoetbal%2Ftrainingspakken%2F%3Fpa_merk%3Dnike\" target=\"_blank\" rel=\"sponsored nofollow\">\r\n<img src=\"https://ti.tradetracker.net/?c=9750&amp;m=1288070&amp;a=398285&amp;r=&amp;t=html\" alt=\"\" width=\"568\" height=\"100\" border=\"0\"></a>', '2021-03-19 13:29:27', '2021-09-24 16:06:32'),
(19, 'Adidas korting tot 50%', 1, 300, 150, 'Adidas-korting-tot-50', '20210924180555.png', '<p>didas en Nike zijn de grote concurrenten van elkaar. Samen strijden zij voor de hoogste positie onder de mensen een strijd die blijkbaar lang door zal duren. Maar als we praten over strijd en we zouden nu moeten kiezen dan wordt het wel erg moeilijk. Adidas komt met 50% korting op producten.</p><h2>Adidas korting</h2><p>Adidas presenteert nu kortingen waarvan je zegt:\"Ho maar, dit lijkt mij te mooi om waar te zijn?!\" Inderdaad het zijn geweldige kortingen die heel veel aandacht zullen trekken de aankomende tijden. Als je het vergelijkt met Nike heeft Nike op dit ogenblik niet zulke immense kortingen als hoe Adidas die nu heeft.</p><h2>50% korting Adidas</h2><p>Bij Adidas heb je de mogelijkheid binnen een bepaald assortiment 50% te krijgen op geweldige aandelen. Het kunnen schoenen zijn, kleding artikelen zijn of misschien zelfs wel accessoires. Tis maar net waar je voorkeur naar uitgaat! Het zijn ongekende tijden die zeker handig uitkomen voor mensen die het niet zo breed hebben op dit ogenblik</p><h2>Nieuwsbrief 20% korting</h2><p>Ben je nog niet ingeschreven voor de nieuwsbrief van Adidas? Dan mis je een geweldige korting! Ze bieden hun klanten 20% korting voor hun die zich inschrijven voor nieuwsbrieven. Zij geven jou dan alle acties te kennen die voor jou zo aantrekkelijk zijn dat je ze bijna niet kan nalaten te kopen (mits je geïnteresseerd bent) en daarnaast geven ze je nog een korting op jouw bestelling? Mooier dan dat bestaat er niet!</p><h3><span style=\"background-color: rgb(255, 255, 0);\"><font color=\"#000000\">Bezoek de actie:</font></span></h3><a href=\"https://tc.tradetracker.net/?c=31384&amp;m=1751834&amp;a=398285&amp;r=&amp;u=\" target=\"_blank\" rel=\"sponsored nofollow\"><img src=\"https://ti.tradetracker.net/?c=31384&amp;m=1751834&amp;a=398285&amp;r=&amp;t=html\" alt=\"\" width=\"728\" height=\"90\" border=\"0\"></a>', '2021-03-19 15:14:37', '2021-09-24 16:05:55'),
(20, 'Korting op al jouw Blokker producten tot 5%', 19, 300, 150, 'Korting-op-al-jouw-Blokker-producten-tot-5', '20210924180210.jpg', '<p>Blokker is één van de grootste Nederlandse keten van huishoudelijke spullen en producten. Het heeft in totaal 430 Filialen in Nederland. Afgezien van deze omschrijving bieden zij ook geweldige acties aan. Je kan van alles bij Blokker halen: van mixers tot aan stofzuigers, borden, pannen of strijkijzer verzin het maar. Zij hebben het!</p><h2>2e halve prijs</h2><p>Sinds kort is Blokker begonnen met geweldige acties namelijk: de 2e halve prijs. Zoals al hier boven aangegeven bieden zij van alles aan qua assortimenten. Daarom is het nu het moment om nu dingen in te slaan die je anders niet zou hebben. Heb je een pan die bijvoorbeeld niet goed functioneert haal hem dan nu nog nu het nog kan!&nbsp;</p><h2>Gratis verzending</h2><p>Ook heb je recht op gratis verzending als je bestelling meer bedraagt dan €39,95, daar ben je zo over heen! Daarnaast bieden zij jou en mij onze producten af te leveren binnen 1-2 werkdagen. Mooier dan dat dit ga je het niet vinden. En een snelle tussendoor bieden zij ook 30 dagen bedenktijd/retouren.</p><h2>Grijp de actie die zij bieden!</h2><span><p>Maak deel uit van deze actie nu het nog kan.&nbsp;</p><h2><br></h2><a href=\"https://www.blokker.nl/prijspakkersfestival-krassen.html\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></span><p></p>', '2021-09-10 04:54:24', '2021-09-24 16:02:10'),
(21, 'Geweldige acties bij Mediamarkt! Tot 20% korting', 4, 300, 150, 'Geweldige-acties-bij-Mediamarkt!-Tot-20-korting', '20210924180508.png', '<p>Mediamarkt bied geweldige acties aan nu in de maand september! Heb jij ze al gezien? Mediamarkt is een Duitse winkelketen die het meest gespecialiseerd is in dingen als elektronica. Wie kent MediaMarkt nou niet? ,,Mediamarkt ik ben toch niet gek?!\" We hebben allemaal welis gehoord die slogan.</p><h2>Mega deals</h2><p>Mediamarkt brengt in de markt mega deals naar voren; deals waar jij en ik van schrikken! Je kunt voor één artikel €84 euro besparen. Dat is enorm! Je hebt ook verschillende producten die zij afprijzen. Zo heb je bijvoorbeeld een TV die €649 kost maar is afgeprijsd naar €488! Dat zijn immense afgeprijsde bedragen die je echt niet voorbij moet laten gaan! Deze actie is geldig tot en met 12 september!&nbsp;</p><h2>Termijnen betalen? Geen probleem!</h2><p>Wil je liever een product in termijnen betalen ook die mogelijkheid biedt Mediamarkt zijn klanten. Het is heel eenvoudig kies een product naar keuze en bij het afbetalen kun je ervoor kiezen om het in termijnen te betalen. Simpel en eenvoudig noemen ze dat!</p><h2>Kwaliteit</h2><p>Wij kennen Mediamarkt als de winkelketen van de kwaliteit. We praten nu niet over iets alledaags maar het is wel Mediamarkt die zulke geweldige aanbiedingen en kortingen naar voren brengt. Kortingen die je niet zomaar voorbij zie komen. Maar die exclusief beschikbaar zijn bij Mediamarkt. Beter dan dit zul je niet vinden! Wie kan namelijk opwegen tegen Mediamarkt?</p><h2>Vanaf 10% korting tot 50% korting</h2><p>Maak snel kans op deze geweldige kortingen via Mediamarkt.nl Neem nu nog deel aan deze banner!</p><p><span><a href=\"https://www.mediamarkt.nl/nl/shop/mega-deals.html?rbtc=tra|con|2356989|9faf4126af1d62f7fe09ff39bae0350f|p|&amp;tduid=9faf4126af1d62f7fe09ff39bae0350f\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></span></p><p></p>', '2021-09-10 05:49:48', '2021-09-24 16:05:08'),
(22, 'Op al jouw C&A producten tot 30% korting!', 7, 300, 200, 'Op-al-jouw-CA-producten-tot-30-korting!', '20210924180334.png', '<p>Dat klopt je kunt bij C&amp;A 30% korting krijgen op al jou C&amp;A producten? Opmerkelijk is het toch weer met C&amp;A! Het lijkt maar niet op te houden met geweldige aanbiedingen en acties die zij naar voren brengen!</p><h2>Kortingen</h2><p>Je hebt verschillende kortingen bij C&amp;A van jongens tot aan meisjes, van heren tot aan dames het is er! Meestal is er elke week wel nieuwe artikelen die zijn afgeprijsd het is een stroming van afgeprijsde producten en artikelen! Het houdt nooit op! Zo hebben zij ook nog is een actie waarbij je bij 3 producten een product gratis krijgt!</p><h2>Gratis verzending!</h2><p>Deze actie gaat gepaard met een gratis thuislevering bij aanschaf boven de 39 euro! Niemand houdt er van om zo lang te moeten wachten op je product. Daar weet C&amp;A ook rekening mee te houden. Zij bieden een levertijd aan waarbij voor 13:00 besteld de volgende dag in huis! Wel met uitzonderingen nagelaten (als in het weekend)</p><h2>Grijp jou korting vandaag nog!</h2><p>Wacht niet langer maak kans op jou korting&nbsp;</p><p><span><a href=\"https://www.c-and-a.com/nl/nl/shop/sale?catalogId=14551&amp;storeId=10158&amp;langId=-1003&amp;krypto=dzJUKFMEI5soNGjvo0fKbQQypC%2F3eSD9uB3ltMmzKVNkp3t9hkBcH3SI6A73bkghRT4WVnhxUZEsELuHxYLsJZRznn1%2BUu0gz7AscB8jobd4%2FXkv0%2BXm4o9d4Tip75%2FG\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></span></p><p></p><h3><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#000000\"><span>Informatie Wehkamp:</span></font></span></h3>', '2021-09-10 06:22:25', '2021-09-24 16:03:34'),
(23, 'Geweldige kortingen bij Otto.nl tot 30% korting!', 2, 250, 125, 'Geweldige-kortingen-bij-Otto.nl-tot-30-korting!', '20210924180125.png', '<p>Otto is weer bezig! Vandaag brengt Otto naar buiten verschillende kortingen in diverse categorieën. Otto staat bekend als een postorderbedrijf een Nederlandse webwinkel die eronder bekend staat dat het kwalitatieve producten op de markt brengt!</p><h2>Uitverkoop!</h2><p>Bij Otto kan je altijd terecht als je iets voor een lagere prijs wil kopen. Ze komen elke week met geweldige acties die het jou aangenaam en ook verleidelijk maken om deel te nemen aan deze tijdelijke acties. Het zijn kortingen waar je over kan dromen! Dingen als 10% korting op de gehele assortiment of dingen als 20% over specifieke producten. Alles valt er te vinden</p><h2>Tijdelijke acties</h2><p>Deze acties zijn nooit vrijblijvend ze zijn tot een bepaalde dag of tijd beschikbaar. Daarom is het van noodzaak deze kans nu nog te grijpen. Voordat het te laat is. Het zijn kortingen die je niet elke dag voorbij ziet komen.&nbsp;</p>\r\n<p><span><a href=\"https://www.otto.nl/sale/heren/#t=%7B%22e_source%22%3A%22sale%22%2C%22e_linkname%22%3A%22fst2_1%22%2C%22e_campaign%22%3A%22sale_fst2_1%22%2C%22e_nav%22%3A%22sale%22%2C%22e_mchannel%22%3A%22interne_campagne%22%7D\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></span></p><p></p><h3><br></h3>', '2021-09-16 05:08:09', '2021-09-24 16:01:25'),
(27, 'Ontdek wat Bjorn Borg aanbiedt aan korting! 30% korting', 1, 300, 150, 'Ontdek-wat-Bjorn-Borg-aanbiedt-aan-korting!-30-korting', '20210924190402.jpg', '<p>Bjorn Borg is een van de bekendste merken die vandaag rondgaan. Bjorn Borg is ook een geliefd merk onder de jongeren als zowel ouderen. Wat Bjorn Borg zo speciaal maakt is zijn naam alleen. \"Bjorn Borg\" De naam alleen trekt heel veel mensen naar deze merk hoewel het geen speciale logo of teken nodig heeft is alleen de naam Bjorn Borg al genoeg. </p><p>Bjorn Borg is ontdekt door een oud Zweeds tennisser hij was in de jaren \'70 de beste tennisser ooit. Hij boekte resultaten na resultaten totdat hij met pensioen ging en een merk begon namelijk zijn eigen naam \"<b>Björn Rune Borg</b>\" En zoals je al kan raden toendertijd als je hoog in het vaandel zit wilt ook iedereen alles van jou hebben!</p><h2>Korting Bjorn Borg</h2><p>De website Bjorn Borg brengt vele kortingen naar zijn klanten. Die kortingen kunnen variëren van 10% korting tot aan 30% korting. En we kennen allemaal de Blackfriday momenten wel dat prijzen zelfs kunnen oplopen tot 70% korting. Gekker kan het niet worden. Maar aangezien Bjorn Borg nog steeds een aanzienlijk goede merk die niet lijkt te dalen qua populariteit lijken de prijzen nogal duur te zijn. </p><p>Maar geen paniek Bjorn Borg brengen vele acties naar voren die een merk als Bjorn Borg toch betaalbaar maken voor iedereen. Wij brengen jou deze unieke code + actie mee. </p><p><br></p><p>		  <a href=\"#\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></p>', '2021-09-24 17:02:29', '2021-09-24 17:04:02'),
(28, 'Krijg bij H&M 10% korting op al jou producten', 6, 500, 150, 'Krijg-bij-HM-10-korting-op-al-jou-producten', '20211001193329.jpg', '<p>Wie kent de welbekende H&amp;M nou niet? Een ontwerp die zeer geliefd is bij de mensen. En dat vanwege zijn kwalitatieve producten en hun zeer lage prijzen waardoor het voor veel mensen aantrekkelijk is om deel te kunnen nemen aan de acties die H&amp;M met zich mee brengt.</p><p> En dan zijn het niet eens altijd acties soms kunnen zij omdat zij nou eenmaal van een bepaald type stof zijn gemaakt of als zij op wat voor \r\nmanier anders gemaakt zijn zeer goedkoop zijn. En dan zijn zij niet eens afgeprijsd. Want als we daarnaartoe gaan wordt het gekke huis.</p><p>Voor wie hem niet kennen is H&amp;M ook ontworpen net als Bjorn Borg door \r\nZweedse ontwerpers. Zij brachten dit product in Zweden voort en vervolgens daarna in Duitsland, Amerika, Canada, China etc. En als laatst ook hier in Nederland!</p><h2>Korting H&amp;M</h2><p>Aangezien vele bezoekers van GratisNetwerk H&amp;M liefhebbers zijn bieden wij jou een exclusieve pagina link aan die zeer goedkope artikelen afprijzen en daarmee jij zo goedkoop mogelijk kan winkelen op basis van deze geweldige kortingen die toegevoegd zijn op deze artikelen. Deze kortingen kunnen oplopen tot 10-30% procent het hangt maar net van af welke dag het is.</p> <p>Wacht niet langer en ontdek deze kortingen nu!<br></p><p>		  <a href=\"https://gratisnetwerk.nl/admin/article#\" class=\"knop\" target=\"_blank\">Pak deze korting nu!</a></p><p><br></p>', '2021-09-24 17:14:25', '2021-10-01 17:33:30'),
(29, 'Nike schoenen met korting 10% korting', 10, 300, 200, 'Nike-schoenen-met-korting-10-korting', '20210928154031.png', '<p>Nike schoenen met korting lijkt misschien iets wat te mooi is om waar te zijn. Maar toch is het zo dat deze kortingen vrijwel vaker voorkomen en dat niet via willekeurige webshops maar ook zelf Nike doet aan deze verschillende acties. Vaker dan gewoonlijk.</p><h2>10% korting op Nike<br></h2><p>Het vinden van deze acties is zeldzaam en niet altijd vanzelfsprekend maar toch zijn er webshops die op een of ander manier toch voor een laag aanzienlijk prijs Nikes weten te verkopen voor een prijs waarvan je denkt: Hoe is dat toch mogelijk? Kortingen die je van ver nooit had zien aankomen. En zoals we alreeds vermelden op onze website: Nike schoenen met korting tot 10% korting. Bieden wij jou de mogelijkheid om deel te nemen aan deze tijdelijke actie.</p><h2>Tijdelijk<br></h2><p>Zoals we alreeds vermelden is deze actie tijdelijk. We weten zelf niet hoelang deze actie nog zal gelden maar grijp deze kans snel voordat het voorbij is. Wij proberen altijd verschillende soorten acties bij elkaar te krijgen zodat jij voor een aanzienlijk prijs toch goedkoop kan winkelen!</p><p>		  <a href=\"#\" class=\"knop\" target=\" _blank\"=\"\">Pak deze korting nu!</a></p><p></p>', '2021-09-28 13:32:19', '2021-09-28 13:40:31'),
(31, 'Deze Adidas actiecode 20% korting', 11, 300, 200, 'Deze-Adidas-actiecode-20-korting', '20210928161531.png', '<p>Wij kennen allemaal Adidas wel. De sneakers die iedereen wilt hebben. Tegenwoordig zijn heel veel mensen bereid om er ook heel veel voor uit te trekken. Het is soms bijzonder hoe de Jeezy\'s tegenwoordig onder de jongeren aanzienlijk goed verkopen. En dat nog wel de maker er niet te veel reclame voor maakt. Het verkoopt zichzelf. </p><p>En dan spreken we niet eens echt alleen over het merk Jeezy\'s maar verschillende soorten normale gympies die wij allemaal weleens hebben gedragen. Adidas brengt een actiecode met zich mee. Bij een aankoop van een product kun je gebruik maken van deze speciale code en het zal dienen tot vermindering van het gevraagde bedrag. </p><p>Deze actiecode die tussen de 20% zit qua korting is vrijwel aantrekkelijk en het zou ons niet verbazen als deze code niet meer geldig zal zijn. Daarom sporen wij al te maar veel mensen erop aan om snel gebruik te maken van deze code.</p><h2>Gebruik snel!<br></h2><p>Ook biedt Adidas zijn klanten de mogelijkheid door middel van een nieuwsbrief dat jij korting kan scoren. Maak snel deel van de nieuwsbrief die Adidas uitbrengt! Ook daar hangen kortingen aan, die zelfs kunnen oplopen tot 40% korting op diverse producten. Wel gelden ze meestal natuurlijk niet voor de gehele assortiment. </p><p>  <a href=\"#\" class=\"knop\" target=\" _blank\" =\"\"=\"\">Pak deze korting nu!</a></p>', '2021-09-28 14:14:01', '2021-09-28 14:15:31'),
(32, 'Wehkamp korting! Bekijk hier!', 3, 300, 200, 'Wehkamp-korting!-Bekijk-hier!', '20210928164154.jpg', '<p>Wehkamp is één van de grootste warenhuis in Nederland. Groter dan Wehkamp ga je niet vinden in Nederland. Natuurlijk zijn er ook gelijke als Otto en Bonprix die ook heel goed doen in de markt is Wehkamp toch wel iets voortreffelijker. </p><p>Bij Wehkamp prijzen ze meestal elke week een categorie kleren af en dat voor een aanzienlijk prijs. Een prijs waar je niet gauw nee op zegt. Hoewel dit zo is gebeurt het niet vaak dat je korting kan krijgen op het gehele assortiment. Dat vind je niet zo gauw. Maar toch kun je hem dit keer wel krijgen!</p><h2>Actiecode</h2><p>Door middel van deze actiecode: WHK2021</p><p> kun je goed scoren bij je volgende bestelling bij Wehkamp. Wil je meedoen aan deze actie? Wees dan niet te laat. Wehkamp geeft deze acties niet zo snel vrij daarom sporen we je aan des te meer vaart te maken. </p><p></p><a href=\"#\" class=\"knop\" =\"\"=\"\">Pak deze korting nu!</a><p></p>', '2021-09-28 14:37:59', '2021-09-28 14:41:54'),
(33, 'Nike Jordan | 10% korting', 10, 300, 200, 'Nike-Jordan-10-korting', '20210928182241.png', '<p>Het verkrijgen van de Nike Jordan is tegenwoordig iets wat heel geliefd is onder de jongeren. Deze schoen kan je al heel moeilijk krijgen voor een bedrag gemiddeld tussen de €50-70. Dit is omdat het vernoemd is naar Michael Jordan. Werelds best spelende basketballer. Wij maken het voor jou mogelijk dat je de Nike Jordan kan krijgen.</p><h2>Jordans</h2><p>Zoals hierboven alreeds vermeld is de Jordans vernoemd naar Michael Jordan. De Nike Jordans zijn schoenen die op het oog er heel kinderlijk uit zien en ook vrijwel simpel maar toch zijn ze zo geliefd onder de jongeren. \r\nDe reden waarom wij het toeschrijven als kinderlijk en dit niet negatief bedoeld is puur omdat het erg aanlokkelijk is voor jongeren. Vooral vanwege zijn kleuren zijn de Nike Jordans populair.</p><h2>Variaties</h2><p>Je hebt verschillende soorten variaties binnen de merk Nike Jordan maar dat bestaat niet uit een hele andere schoen maar puur de kleur is heel anders ingedeeld. Je hebt mensen die liever van groen houden of van roze die kleuren zijn beschikbaar gemaakt voor de Nike Jordan maar dan ook zodanig in elkaar gezet waardoor je het wel wilt hebben! </p><h2>Korting Nike Jordan</h2><p>Wij bieden jou Nike Jordan\'s voor een aanzienlijk goede prijs. Een prijs die je niet kan missen. Deze korting is exclusief te vinden via de onderstaande link die te verkrijgen is via de knop:,,\". Zij wisten voor een aanzienlijk goede prijs toch wel mooie Jordans te scoren en dat voor een aanzienlijk goede prijs. Welke Nike Jordan zul jij kiezen? Je kunt hem nu voor 10% korting krijgen</p>', '2021-09-28 16:20:18', '2021-09-28 16:22:41'),
(34, 'Nike trainingspak | Met vele kortingen! 20% korting', 10, 200, 135, 'Nike-trainingspak-Met-vele-kortingen!-20-korting', '20211004163712.png', '<p>Iedereen droomt erover om een coole pak te hebben als de Nike. Iedereen wilt hem misschien wel maar kan er niet altijd het geld erover hebben. Tegenwoordig zijn verschillende soorten modellen in de markt gekomen die het nog aantrekkelijker maken om te gaan voor een Nike trainingspak of misschien wil je liever broek of de bovenvest. Maar kan ik één krijgen zonder zoveel te hoeven te betalen?</p><h2>Nike trainingspak</h2><p>Het beantwoorden van deze vraag is vrij simpel. Zoek een website die dezelfde trainingspak aanbied dan goedkoper. Tegenwoordig staan mensen niet echt stil dat sommige websites het bewust wat duurder verkopen dan je kan kopen bij je buurman ernaast. Er zijn verschillende factoren waarom hun Nike trainingspak veel duurder is dan van hun buurman.</p><p>Kan te maken hebben omdat hun website veel bezocht wordt en daarom dus geliefd is onder de bezoekers of vanwege hun layout en het gemak die de website levert tot aankoop van producten. Veel factoren aan verbonden maar één ding is zeker: hij is er te vinden! Je moet weten ze kopen hem allemaal voor een inkoopprijs en verkopen hem voor een bepaald bedrag door zodat zij winst kunnen maken. </p><h2>20% korting op Nike Trainingspak?</h2><p>Het is mogelijk en zeer realistisch. Zoals hierboven al vermeld als jij wilt kijken of je misschien wel een mooie Nike Trainingspak kan vinden dan bieden we jou deze mogelijkheid. Maak gebruik van de onderstaand link die brengt je naar de goedkoopste maar toch wel kwalitatieve trainingspakken!<br></p><p></p><a href=\"#\" class=\"knop\" =\"\"=\"\">Pak deze korting nu!</a><p></p>', '2021-10-04 14:33:55', '2021-10-04 14:37:12'),
(35, 'H&M oktober actie | 20% korting', 6, 200, 100, 'HM-oktober-actie-20-korting', '20211004164602.jpg', '<p>H&amp;M brengt vandaag exclusieve acties naar buiten en dat lucratieve manier die voldoet aan de verwachtingen van de klanten. Zoals jij en ik al weten verkeren wij toch wel in een bijzonder tijd. COVID heeft ervoor gezorgd dat wij alles online moesten bestellen maar H&amp;M wist altijd meegaand te zijn met de gemaakte maatregelen</p><h2>H&amp;M + Korting = Besparen</h2><p>We houden allemaal van kortingen omdat het ons altijd helpt door niet te veel uit te geven voor zaken waar je meer voor moest leggen maar toch voordelig kan krijgen. Het is simpel bij H&amp;M brengen ze een nieuwsbrief naar voren waar jij je simpelweg bij kan aanmelden door middel van aanmelding maak je jij kans op voordelige acties die wellicht met tijdslot verbonden zijn. </p><p>Waardoor jij nooit deze acties kan missen. Ook brengen ze via dit systeem verschillende kortingscodes naar voren of laten zij weten welke geweldige acties je vandaag kan krijgen. Daarom maak juist des te meer deel aan deze systeem Nu het nog kan!</p><h2>Maak nu kans!</h2><p>Maak vandaag kans van deze oktober deal! Er wachten jou nog te gekke acties op<br></p><p></p><a href=\"#\" class=\"knop\" =\"\"=\"\">Pak deze korting nu!</a><p></p>', '2021-10-04 14:44:45', '2021-10-04 14:46:02'),
(36, 'Otto brengt mega kortingen | 30% korting', 2, 200, 100, 'Otto-brengt-mega-kortingen-30-korting', '20211005080225.png', '<p>Otto brengt vandaag mega korting naar voren. Kortingen waarvan je zou kunnen dromen! Otto is zoals we alreeds hebben bekend gemaakt staat Otto als postorderbedrijf die deel uitmaakt aan verschillende categorieën aan kleding. Van kleding tot aan sokken. Alles is te vinden bij Otto.</p><h2>Mega kortingen</h2><p>Alleen vandaag nog maak je kans maken op geweldige kortingen bij Otto. Deze kortingen zijn niet vrijblijvend en zullen niet voor altijd gelden. Maak daarom snel aanstalten door in aanmerking te komen aan deze geweldige kortingen.&nbsp; Deze megakortingen ga je niet altijd terug zien! Kijk voor meer informatie op Otto.nl. Wil je snel gebruik maken van deze mega kortingen maak dan deel van de onderstaande knop</p><p></p><a href=\"#\" class=\"knop\" =\"\"=\"\">Pak deze korting nu!</a><p></p>', '2021-10-05 05:53:52', '2021-10-05 06:02:25');
INSERT INTO `articles` (`id`, `title`, `category_id`, `width`, `height`, `slug`, `image`, `content`, `created_at`, `updated_at`) VALUES
(38, 'Beslagvrijevoet overgenomen', 20, 200, 250, 'Beslagvrijevoet-overgenomen', '20220711204534.jpg', '<p>De huidige berekening van de beslagvrije voet is \r\ningewikkeld en moeilijk uitvoerbaar. Dat komt omdat burgers hiervoor \r\nzelf veel informatie moeten aanleveren. En dat gebeurt nu zelden. In \r\n2018 waren er 265.000 beslagen op periodieke inkomsten (salaris en \r\nuitkering). In 75% van deze loonbeslagen wordt de beslagvrije voet nog \r\nte laag vastgesteld. Daardoor houden bijna 200.000 burgers met schulden,\r\n te weinig geld over voor hun primaire levensbehoeften. Het gevaar is \r\ndat de schuldsituatie van betrokkenen verder escaleert.<div class=\"grid__item-xs--col-12 grid__item-md--col-6\"></p>\r\n          <h2>Wat is er gedaan?</h2>\r\n          <p>De Koninklijke Beroepsorganisatie van Gerechtsdeurwaarders (KBvG) heeft in 2014 een\r\n            wetsvoorstel\r\n geschreven en aan het ministerie van Sociale Zaken en Werkgelegenheid \r\naangeboden. Met dat voorstel wordt de beslagvrije voet snel en goed \r\nberekend. Bovendien is de berekening dan ook door de burger in te zien.\r\n          </p>\r\n          <p>Dit voorstel is in het voorjaar van 2017 als wet aangenomen en zou op 1 januari 2019 worden ingevoerd.</p><div class=\"grid__item-xs--col-12 grid__item-md--col-6\">\r\n          <h2>Wat is er <span>niet</span> gedaan?</h2>\r\n          <p>Door ICT-problemen bij de overheid is de invoering van de \r\nwet uitgesteld tot 1 januari 2021. De invoering dreigt nu opnieuw te \r\nworden uitgesteld naar een nog later tijdstip!.</p>\r\n          <p>Die ICT-problemen hebben te maken met het ontwikkelen van \r\neen rekenmodule, een website voor de burger (inlogportaal) en een \r\nvoorziening waarmee de benodigde informatie kan worden opgehaald. \r\nGerechtsdeurwaarders hebben dit allemaal al klaar en hebben de overheid \r\naangeboden om dit te gebruiken. Maar de overheid wil dit zelf gaan \r\nontwikkelen, dat kost tijd en daardoor ontstaat nog meer vertraging bij \r\nhet invoeren van de wet.</p><section class=\"section section--blue\">\r\n    <div class=\"section__inner\">\r\n      <div class=\"grid-\">\r\n        <div class=\"grid__item-xs--col-12 text-center\">\r\n          <h2>Nog meer uitstel kan niet langer!</h2>\r\n          <p>De overheid is de vertragende factor om de vereenvoudigde \r\nbeslagvrije voet in te voeren. De KBvG vindt dat dit echt niet langer \r\nkan, omdat verder uitstel ervoor zorgt dat nog meer burgers in ernstige \r\nfinanciele problemen komen.</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section><p></p>\r\n        </div><p></p>\r\n        </div><p></p>\r\n        <p></p>', '2022-07-11 19:41:27', '2022-07-11 19:45:34');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Acties', '2021-02-02 12:57:07', '2021-02-02 12:57:07'),
(2, 'Otto', '2021-02-02 12:57:12', '2021-02-02 12:57:12'),
(3, 'Wehkamp', '2021-02-07 15:00:06', '2021-02-07 15:00:06'),
(4, 'Mediamarkt', '2021-03-01 16:41:26', '2021-03-01 16:41:26'),
(5, 'Bonprix', '2021-03-01 16:41:32', '2021-03-01 16:41:32'),
(6, 'H&M', '2021-03-01 16:41:38', '2021-03-01 16:41:38'),
(7, 'C&A', '2021-03-01 16:41:45', '2021-03-01 16:41:45'),
(10, 'Nike', '2021-03-01 16:42:02', '2021-03-01 16:42:02'),
(11, 'Adidas', '2021-03-01 16:42:07', '2021-03-01 16:42:07'),
(12, 'Puma', '2021-03-01 16:42:13', '2021-03-01 16:42:13'),
(19, 'Blokker', '2021-09-10 04:56:18', '2021-09-10 04:56:18'),
(20, 'Overig', '2022-07-11 19:37:09', '2022-07-11 19:37:09');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `footers`
--

CREATE TABLE `footers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footers`
--

INSERT INTO `footers` (`id`, `name`, `category`, `link`, `created_at`, `updated_at`) VALUES
(4, 'Adidas korting 50%', 'Acties', 'https://gratisnetwerk.nl/home/Acties/Adidas-korting-tot-50%25', '2021-03-19 15:21:42', '2021-03-26 16:52:08'),
(5, 'ChristMedia', 'Links', 'https://christmedia.nl/home/', '2021-03-19 15:53:31', '2021-03-19 15:53:31'),
(7, 'Op al jouw C&A producten tot 30% korting', 'Laatste updates', 'https://gratisnetwerk.nl/home/C&A/Op-al-jouw-CA-producten-tot-30-korting!', '2021-09-10 06:41:40', '2021-09-24 16:18:29'),
(8, 'Geweldige acties bij Mediamarkt! Tot 20% korting', 'Laatste updates', 'https://gratisnetwerk.nl/home/Mediamarkt/Geweldige-acties-bij-Mediamarkt!-Tot-20-korting', '2021-09-10 06:41:55', '2021-09-24 16:18:54'),
(9, 'Korting op al jouw Blokker producten tot 5%', 'Laatste updates', 'https://gratisnetwerk.nl/home/Blokker/Korting-op-al-jouw-Blokker-producten-tot-5', '2021-09-10 06:42:13', '2021-09-24 16:19:13'),
(10, 'Acties', 'Categories', 'https://gratisnetwerk.nl/home/Acties', '2021-09-10 06:42:45', '2021-09-10 06:42:45'),
(11, 'Wehkamp', 'Categories', 'https://gratisnetwerk.nl/home/Wehkamp', '2021-09-10 06:43:02', '2021-09-10 06:43:02'),
(12, 'Bonprix', 'Categories', 'https://gratisnetwerk.nl/home/Bonprix', '2021-09-10 06:43:17', '2021-09-10 06:43:17'),
(13, 'Nike', 'Categories', 'https://gratisnetwerk.nl/home/Nike', '2021-09-10 06:43:27', '2021-09-10 06:43:27'),
(14, 'Blokker', 'Categories', 'https://gratisnetwerk.nl/home/Blokker', '2021-09-10 06:45:11', '2021-09-10 06:45:11'),
(15, 'Tot 70% korting nike voetbalschoenen', 'Acties', 'https://gratisnetwerk.nl/home/Nike/Tot-70%25-korting-nike-voetbalschoenen', '2021-09-10 06:45:37', '2021-09-10 06:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `headers`
--

CREATE TABLE `headers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `headers`
--

INSERT INTO `headers` (`id`, `name`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'https://gratisnetwerk.nl', '2021-02-04 17:16:48', '2021-02-04 17:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2020_12_28_202754_create_articles_table', 1),
(7, '2020_12_28_214331_create_headers_table', 1),
(8, '2020_12_29_154345_create_sessions_table', 1),
(9, '2021_01_03_134150_create_footer_table', 1),
(10, '2021_01_11_123730_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('1Ya2jS3M9R3AUh9bk6a93gAs2hZB73T52oynteg5', NULL, '120.41.45.199', 'Mozilla/5.0 (compatible; ThinkChaos/0.3.0; +In_the_test_phase,_if_the_ThinkChaos_brings_you_trouble,_please_add_disallow._Thank_you.)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoibjhPUnVaMmNwdmN1a3VycjNBYjkwUXVmSVRUVHpaOGNSbUgxQ0hVWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjQ6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1657649191),
('2Oudklo2ztSzLH373jaX3yOqzibEANP9FKSisZ1T', NULL, '176.53.216.113', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiQ1ptWG40cDE0RUMwWHI1NlE5UlRrbUQzZUlnT0dvazdIVGt3WnFKRCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vbWFpbC5ncmF0aXNuZXR3ZXJrLm5sIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657648709),
('6phxveKyrDdDRgC6pQtUBWJndoALJo5LuwotKm7i', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMG1uMkJWbWh2THkxdFAzUDhlVjZaMnpuVG9IT25vdVJsa1VDRVpjbCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly9ncmF0aXNuZXR3ZXJrLm5sL2xvZ2luIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655744),
('CkhTVx2OcsXiFZWIUDbYu8BdKPRfrHV7LAPOHvm8', NULL, '54.36.148.13', 'Mozilla/5.0 (compatible; AhrefsBot/7.0; +http://ahrefs.com/robot/)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiUzlrWU9tejJUR1lFeTJWRVdqMm13a3U3WExNVDZHMU5UVEVvb3pBaSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTc6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lL0Jlc2xhZ3ZyaWpldm9ldC1vdmVyZ2Vub21lbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1657653939),
('DEabNrzt5IaWAqX2jjvESqiMBxlMPq87AK8X9dIM', 1, '39.42.113.200', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiTlBSaXFNODZyYzIyN2VIRGw1dUZlSDhUS3d0ZFF6bGJHZjNJNVZEMyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzg6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9hZG1pbi9hcnRpY2xlIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJHMxSTk3Mkp6ZzZqcVpRQi44R25EbnV5MnRJMEh5SVlzTGhOTHFyTndRQTB6YkNveGRKNlhPIjt9', 1657655794),
('GDaS69DQ3sHdjiVARDfpLURGBjgyPCyrIiCN9sXf', NULL, '171.13.14.12', 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3765.0 Mobile Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoidWxBclJRRVB1bnZuUjlCUUxFSmhkdzlLc0luTXk3WVpKTHA4N3RDRCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjg6Imh0dHA6Ly9ncmF0aXNuZXR3ZXJrLm5sL2hvbWUiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1657654081),
('grbEAOUf6By6IMBZHV3pZhWu051hjXOkKoqdX8gK', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoidlNVRVIwTHp2SkZZZkh0Rlp1azRmUUtkT0I0cDAwWmJLaGpjdlVtYSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655764),
('lKBPmZ99gx9BopoWk1V1Ov2VrIIandWEpZnTfXjb', NULL, '89.104.110.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRzR2Y25HZWt3M3Y3NENaZm9NR0xaZDl2WXJvaW00ZUswelFrVWtxVyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzQ6Imh0dHBzOi8vbWFpbC5ncmF0aXNuZXR3ZXJrLm5sL2hvbWUiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1657648710),
('lZSp5HWmLZ4v6cjbve1qKfgKCXXK6pEYoDdJLMdW', NULL, '120.41.45.199', 'Mozilla/5.0 (compatible; ThinkChaos/0.3.0; +In_the_test_phase,_if_the_ThinkChaos_brings_you_trouble,_please_add_disallow._Thank_you.)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoicXRtbmhjbVRLdXdQdWhWdEk2enBkTlpCZzZ2M0hqT1pxcWx2OUt0cSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657649203),
('miMrqP5SdvWp7swPSvn28ejVKFBSHkNzOUceGJcQ', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoicG1Qd0tnbU9WQjZYalZ6OWo4cW9uWjVKV1RWZjdxbXg2ekZRYnUzMyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjM6Imh0dHA6Ly9ncmF0aXNuZXR3ZXJrLm5sIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655755),
('mUIekPOQ3N8n7XlqUlHLgvzX8ciASnwhv0eozNjj', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiR1lJaURzVTZsbXBNV0Q1djB2WGcyT0xsdnZPUWVmWVVCQ09LeW10diI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655752),
('NcLWAtvSMtt4Luqt5OTjAn6yAaLjNghjelbCfiXn', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoieW5zMEVTVTJsT0ZhOGo2cURWWW5yOUZ1c1RTVzhMYnVQZHI0U1pxdSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655743),
('nLPppJkOltjn0cXqRmp5jbFY7wRDB0N1L6zTL285', NULL, '89.104.101.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoibzduRTZubGcySFNLVmszUjhCQ0IyTXptakJzN3dFNU5QMW9CMm50SCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHBzOi8vd3d3LmdyYXRpc25ldHdlcmsubmwvaG9tZSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1657655559),
('oIvVaMzj9cUZvLykpAHHIdq6dZvRLCnOKV5lRiOr', NULL, '120.41.45.194', 'Mozilla/5.0 (compatible; ThinkChaos/0.3.0; +In_the_test_phase,_if_the_ThinkChaos_brings_you_trouble,_please_add_disallow._Thank_you.)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiSGFMcFFqRGlCRjUxU2hhU2NtU3hXSW4zbmJaQXNYRDdRaFA5V3lVYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657648792),
('pPgSeyFxECCudZIDz0aXQajFFMK5XlAaQjrDsncd', NULL, '185.191.171.23', 'Mozilla/5.0 (compatible; SemrushBot/7~bl; +http://www.semrush.com/bot.html)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiTUhhekUwd3VHTmNhcXhBM3RTOUpUUXdDZ1E5VmdGaXZ6ZzNIZVRXSiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzQ6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lL1B1bWEiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1657655223),
('SE6PFCwqEdLGihGvQrD2iwH7ZWhfHH1EmLQ4cJyn', 1, '149.5.4.224', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoidVBnUjNhU3BCNDdFMmEwYUp6TG1MUURITXVxUUQyT0dodjc1VzVnWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJHMxSTk3Mkp6ZzZqcVpRQi44R25EbnV5MnRJMEh5SVlzTGhOTHFyTndRQTB6YkNveGRKNlhPIjt9', 1657649466),
('tUtYRSIbEVQFJAc7GsgtWUNW576WpQuPFNq3SeaG', NULL, '35.84.134.94', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.6.01001)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiNm5uclE5M0gyWUowZlFqa1NHT056M2szZHF0bkk5a3poUHl3Sk02dyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjg6Imh0dHA6Ly9ncmF0aXNuZXR3ZXJrLm5sL2hvbWUiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1657654234),
('UAnWI9KHj7BukAlYqKTI1PPQnbQkgW6CyzqU6ONB', NULL, '45.90.63.219', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoielhUQXNnSFNUWlVRS05oeHdhMUZoV1IxWkhXbTZHQVllcVNtSEJxayI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjg6Imh0dHBzOi8vd3d3LmdyYXRpc25ldHdlcmsubmwiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1657655558),
('UIo6mQRjtFanTZj3syCctCEC5VmEpthNA2wmAXUY', NULL, '120.41.45.194', 'Mozilla/5.0 (compatible; ThinkChaos/0.3.0; +In_the_test_phase,_if_the_ThinkChaos_brings_you_trouble,_please_add_disallow._Thank_you.)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiVzRSVEJHaGRMWHB2QVhwRERzSEpST2YyTHo0Um9kM3c1TmdhQjVFTSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjQ6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1657648788),
('vmpN5uqWuNh9ULTX3XiOoyx4ayY6r8i8gP7nWPLv', NULL, '171.13.14.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiTGZZbjlvbXBlUzJvOHlRdkI4dThYVGJTYTRwSkhkTTdZQWtUUTh1ZyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657654074),
('VY3f7VMRqvwzJ2fWjYywFgrn9lUAIDFHYHLicyUk', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiSlcycUQ4M09ma3RwRkc0cVV6TTQ4YnI4Tk5RQ1A4VzNPcUtLTE45WCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657655743),
('zUSbxJbvWpQBmtH7zAIg6mcmg6VvaqRj6qU2FFbq', NULL, '144.178.79.65', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRkpuNk1MRE5aWXpyTXVZemZ2dzN1aGxQbVFOVjFoRDhlM05jaDV3QiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZ3JhdGlzbmV0d2Vyay5ubC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1657651490);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Jun', 'info@gratisnetwerk.nl', NULL, '$2y$10$s1I972Jzg6jqZQB.8GnDnuy2tI0HyIYsLhNLqrNwQA0zbCoxdJ6XO', NULL, NULL, 'isDZ0DLDUdlCjukZSNdzl5d9dVoC3pbY1CcIcvB7NQWZn575lGiUMCjWFBbS', NULL, NULL, '2021-02-02 12:56:58', '2021-02-02 12:56:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `headers`
--
ALTER TABLE `headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footers`
--
ALTER TABLE `footers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `headers`
--
ALTER TABLE `headers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
