<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Header;
use App\Models\Footer;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category = null)
    {
		if ($category) {
			$pagetitle = $category.' - GratisNetwerk';
		} else {
			$pagetitle = 'GratisNetwerk - het netwerk waar volop kansen liggen!';
		}
		$data['pagetitle'] = $pagetitle;
        $data['searchKey'] = '';
        $data['headers'] = Header::all();
        if ($category) {
			$categoryId = Category::where('name', $category)->first()['id'];
            $data['articles'] = Article::where('category_id', $categoryId)->orderBy('title')->get();
            $data['recentArticles'] = Article::where('category_id', $categoryId)->orderBy('created_at', 'ASC')->take(5)->get();
        } else {
            $data['articles'] = Article::orderBy('created_at', 'desc')->get();
            $data['recentArticles'] = Article::orderBy('created_at', 'ASC')->take(5)->get();
        }

        $categories = Category::all();
        $categoryArr = [];

        foreach ($categories as $category) {
            $category['count'] = Article::where('category_id', $category->id)->count();
            $categoryArr[$category->id] = $category->name;
        }

        $footerCategories = ['Categories', 'Laatste updates', 'Acties', 'Over ons', 'Links'];
        $footers = [];
        foreach ($footerCategories as $footerCategory) {
            $footers[$footerCategory] = Footer::where('category', $footerCategory)->get();
        }
        $data['footers'] = $footers;
        $data['categories'] = $categories;
        $data['categoryArr'] = $categoryArr;
        return view('user.home', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category, $post)
    {
        $data['searchKey'] = '';
        $title = implode(' ', explode('-', $post));
        //$article = Article::where('title', '=', $title)->first();
		$article = Article::where('slug', $post)->first();
		if(!$article){
			return redirect('/home');
		}
        $data['headers'] = Header::all();
        $data['article'] = $article;
        //$data['recentArticles'] = Article::orderBy('created_at', 'ASC')->take(5)->get();
		$data['recentArticles'] = Article::orderBy(DB::raw('RAND()'))->take(5)->get();
        $categories = Category::all();
        $categoryArr = [];

        foreach ($categories as $category) {
            $categoryArr[$category->id] = $category->name;
        }

        $footerCategories = ['Categories', 'Laatste updates', 'Acties', 'Over ons', 'Links'];
        $footers = [];
        foreach ($footerCategories as $category) {
            $footers[$category] = Footer::where('category', $category)->get();
        }
        $data['footers'] = $footers;
        $data['categoryArr'] = $categoryArr;
        return view('user.detail', $data);
    }
}
