<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Header;
use App\Models\Footer;
use App\Models\Category;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($keyword)
    {
        $searchKey = $keyword;
        $data['headers'] = Header::all();
        $articles = Article::where('title', 'like', '%' . $searchKey . '%')->get();
        $data['searchKey'] = $searchKey;
        $data['articles'] = $articles;

        $categories = Category::all();
        $categoryArr = [];

        foreach ($categories as $category) {
            $categoryArr[$category->id] = $category->name;
        }

        $footerCategories = ['Films', 'In ontwikkeling', 'Series', 'Over ons', 'Links'];
        $footers = [];
        foreach ($footerCategories as $category) {
            $footers[$category] = Footer::where('category', $category)->get();
        }

        $data['categoryArr'] = $categoryArr;
        $data['footers'] = $footers;
        return view('user.search', $data);
    }
}
