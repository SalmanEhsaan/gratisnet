<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        foreach ($articles as $article) {
            $article['category'] = Category::find($article->category_id)->name;
        }

        $data['articles'] = $articles;
        $data['categories'] = Category::all();
        return view('admin.article', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = date('YmdHis') . '.' . $request->file('image')->extension();
        $request->file('image')->storeAs('public/images/', $filename);
        if (Article::create([
            'title' => $request->title,
            //'slug' => implode('-', explode(' ', $request->title)),
            'slug' => $this->customUrlEncoder($request->title),
            'image' => $filename,
            'content' => $request->content,
            'width' => $request->width,
            'height' => $request->height,
            'category_id' => $request->category_id,
        ])) {
            return ["status" => 'OK'];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateArticle(Request $request, $id)
    {
        $article = Article::find($id);
        Storage::delete('public/images/' . $article->image);
        $filename = date('YmdHis') . '.' . $request->file('image')->extension();
        $request->file('image')->storeAs('public/images/', $filename);
		$title = $request->title;
        $article->title = $title;
		//$title = str_replace('%', '%25', $title);
        //$article->slug = implode('-', explode(' ', $title));
		$article->slug = $this->customUrlEncoder($title);
        $article->image = $filename;
        $article->content = $request->content;
        $article->width = $request->width;
        $article->height = $request->height;
        $article->category_id = $request->category_id;

        if ($article->save()) {
            return ['status' => 'OK'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        Storage::delete('public/images/' . $article->image);
        if ($article->delete()) {
            return ['status' => 'OK'];
        }
    }
	
	public function customUrlEncoder($string){
		$x='0123456789abcdefghijklmnopqrstuvwxyz';
		$random_string = substr(str_shuffle(str_repeat($x, ceil(10/strlen($x)) )),1,10);
		$string = str_replace(' ','-',$string);
		$string = preg_replace('/[^A-Za-z0-9.!\-]/','',$string);
		$string = preg_replace('/-+/','-',$string);
		
		//$string .= '-'.$random_string;
		return $string;
	}
}
