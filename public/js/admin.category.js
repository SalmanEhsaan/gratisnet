$(document).ready(function () {
    var token = $('meta[name="csrf-token"]').attr('content')

    $('#save').click(function () {
        if ($('#name').val() === '') {
            notification('Warning!', 'Please enter the name of category');
        } else {
            var url = gSiteURL + 'admin/category';
            var method = 'POST';
            if ($('#action').val() !== 'create') {
                url += '/' + $('#selected-category').val();
                method = 'PUT';
            }
            $.ajax({
                url: url,
                method: method,
                data: {
                    _token: token,
                    name: $('#name').val(),
                },
                success: function (res) {
                    if (res.status === 'OK') {
                        window.location.reload();
                    }
                }
            });
        }
    });

    $('table .delete').on('click', function () {
        var categoryId = $(this).closest('tr').attr('tid');
        $.ajax({
            url: gSiteURL + 'admin/category/' + categoryId,
            method: 'DELETE',
            data: {
                _token: token,
            },
            success: function (res) {
                if (res.status === 'OK') {
                    window.location.reload();
                }
            }
        });
    });

    $('table .update').on('click', function () {
        $('#save').text('Update');
        var category = {
            id: $(this).closest('tr').attr('tid'),
            name: $(this).closest('tr').children().eq(1).text()
        }

        $('#action').val('update');
        $('#selected-category').val(category.id);

        $('#category-modal #name').val(category.name);
        $('#category-modal').modal('show');
    });

    function notification(title, content) {
        clearTimeout(during);

        $('.alert strong').text(title);
        $('.alert .alert-content').text(content);
        $('.alert').removeClass('d-none');

        var during = setTimeout(function () {
            $('.alert strong').text('');
            $('.alert .alert-content').text('');
            $('.alert').addClass('d-none');
        }, 3000);
    }

    $("#category-modal").on('hidden.bs.modal', function () {
        $('#name').val('');
        $('#action').val('create');
        $('#selected-category').val('0');
        $('#save').text('Save');
    });
});