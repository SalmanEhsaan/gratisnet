@extends('layouts/user')

@section('title', 'Article manage')
@section('breadcrumb', 'Article manage')

@section('page')
<div class="row mx-0">
  <div class="col-12">
    <div class="artlcles">
      @foreach($articles as $key => $article)
      <div class="article d-flex flex-column py-3">
        <div class="row">
          <div class="col-12 col-lg-12">
            <p class="title">{!!$article->title!!}</p>
            <div class="content">{!!$article->content!!}</div>
            <a href="{{env('APP_URL')}}home/{{$categoryArr[$article->category_id]}}/{{$article->slug}}" class="read-more">
              Bekijk de informatie >>>
            </a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

<script>
  var gSiteURL = "<?php echo env('APP_URL') ?>";
</script>
<script src="{{asset('/js/admin.article.js')}}"></script>
@endsection