@extends('layouts/user')

@section('title', "{$article->title} - GratisNetwerk")
@section('breadcrumb', 'Article manage')

@section('page')
<div class="row mx-0">
  <div class="col-12 col-lg-9">
    <div class="artlcles">
      <div class="article d-flex flex-column py-3">
        <div class="row">
          <div class="col-12">
            <a href="/home/{{implode('-', explode(' ', $article->title))}}" class="hoofdtitel">{{$article->title}}</a>
            <div class="detail content">{!!$article->content!!}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-3 right-side">
    <img src="{{asset('public/storage/images')}}/{{$article->image}}" width="{{$article->width}}" height="{{$article->height}}" style="max-width: 100%">
    @foreach($recentArticles as $key => $recentArticle)
    <div class="article d-flex flex-column py-3">
      <div class="row">
        <div class="col-12 col-lg-12">
          <small class="font-weight-bold">{{substr($recentArticle->created_at, 0, 10)}}</small>
          <p class="title">{{$recentArticle->title}}</p>
          <div class="content">{!!$recentArticle->content!!}</div>
          <a href="{{env('APP_URL')}}home/{{$categoryArr[$recentArticle->category_id]}}/{{$recentArticle->slug}}" class="read-more">
            Bekijk de informatie >>>
          </a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>

<script>
  var gSiteURL = "<?php echo env('APP_URL') ?>";
</script>
<script src="{{asset('/js/admin.article.js')}}"></script>
@endsection