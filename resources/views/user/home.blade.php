@extends('layouts/user')

@section('title', $pagetitle)
@section('breadcrumb', 'Article manage')

@section('page')

<div class="row mx-0 homepage-body">
  <div class="col-12 col-lg-9">
    <div class="artlcles">
      @foreach($articles as $key => $article)
      <div class="article d-flex flex-column py-3">
        <div class="row">
          <div class="col-12 col-lg-8">
            <p class="title">{{$article->title}}</p>
            <div class="content">{!!$article->content!!}</div>
            <a href="{{env('APP_URL')}}home/{{$categoryArr[$article->category_id]}}/{{$article->slug}}" class="read-more">
              Bekijk de informatie >>>
            </a>
          </div>
          <div class="col-12 col-lg-4">
            <img src="{{env('APP_URL')}}public/storage/images/{{$article->image}}" width="{{$article->width}}" height="{{$article->height}}" style="max-width: 100%">
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

  <div class="col-12 col-lg-3">
    <p style="font-size: 25px; color: #00A3E4" class="font-weight-bold mb-0">Categories</p>
    <table class="table">
      <tbody>
        @foreach($categories as $key => $category)
        <tr>
          <td><a href="{{env('APP_URL')}}home/{{$category->name}}">{{$category->name}}</a></td>
          <td class="text-right"><span class="badge badge-pill badge-primary px-2">{{$category->count}}</span></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <p style="font-size: 25px; color: #00A3E4" class="font-weight-bold mb-0">Recente deals</p>
    @foreach($recentArticles as $key => $recentArticle)
    <div class="article d-flex flex-column py-3">
      <div class="row">
        <div class="col-12 col-lg-12">
          <small class="font-weight-bold">{{substr($recentArticle->created_at, 0, 10)}}</small>
          <p class="title">{{$recentArticle->title}}</p>
          <div class="content">{!!$recentArticle->content!!}</div>
          <a href="{{env('APP_URL')}}home/{{$categoryArr[$recentArticle->category_id]}}/{{$recentArticle->slug}}" class="read-more">
            Bekijk de informatie >>>
          </a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>

<script>
  var gSiteURL = "<?php echo env('APP_URL') ?>";
</script>
<script src="{{asset('/public/js/admin.article.js')}}"></script>
@endsection