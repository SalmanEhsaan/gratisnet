@extends('layouts/admin')

@section('title', 'Article manage')
@section('breadcrumb', 'Article manage')

@section('page')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="row">
  <input type="hidden" id="action" value="create" />
  <input type="hidden" id="selected-article" value="0" />

  <div class="col-12 d-flex justify-content-end mb-3">
    <button class="btn btn-success" id="new" data-toggle="modal" data-target="#article-modal">New</button>
  </div>
  <div class="col-12">
    <table class="table">
      <thead>
        <tr>
          <th>No</th>
          <th>Title</th>
          <th>Category</th>
          <th>Avatar</th>
          <th width="400px">Content</th>
          <th>Created</th>
          <th>Manage</th>
        </tr>
      </thead>
      <tbody>
        @foreach($articles as $key => $article)
        <tr tid="{{$article->id}}">
          <td>{{$key + 1}}</td>
          <td>{{$article->title}}</td>
          <td>{{$article->category}} <input type="hidden" class="category-id" value="{{$article->category_id}}"></td>
          <td>
            <p>
              Width: <span class="width">{{$article->width}}</span>px,
              Height: <span class="height">{{$article->height}}</span>px
            </p>
            <img src="{{env('APP_URL')}}public/storage/images/{{$article->image}}" width="150" />
          </td>
          <td style="white-space: pre-line">{!!$article->content!!}</td>
          <td>{{$article->created_at}}</td>
          <td>
            <button class="btn btn-success btn-sm update" data-toggle="modal" data-target="article-modal">Update</button>
            <button class="btn btn-danger btn-sm delete">Delete</button>
          </td>
        <tr>
          @endforeach
      </tbody>
    </table>
  </div>

  <div class="modal" id="article-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title font-weight-bold">New article</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <div class="alert alert-danger d-none">
                <strong></strong>
                <span class="alert-content"></span>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                <label for="title">Title</label>
                <input type="text" id="title" class="form-control" />
              </div>
              <div class="form-group">
                <label for="category">Category</label>
                <select id="category" class="form-control">
                  @foreach($categories as $key => $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="image">Image</label>
                <input type="file" id="image" class="form-control" accept=".jpg, .png, .bmp" />
              </div>
              <div class="form-group">
                <label for="width">Width</label>
                <input type="number" id="width" class="form-control" />
              </div>
              <div class="form-group">
                <label for="height">Height</label>
                <input type="number" id="height" class="form-control" />
              </div>
              <div class="form-group">
                <label for="content">Content</label>
                <textarea id="content" class="form-control w-100"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="save">Save</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var gSiteURL = "<?php echo env('APP_URL') ?>";
</script>
<script src="{{asset('/public/js/admin.article.js')}}"></script>
@endsection