<html>

<head>

  <title>@yield('title')</title>
  <meta name="ahrefs-site-verification" content="2d16d61a0aab8f52703b1df178fc9fe9e4842dfb1c1c1983fdef05a323495bea">
  <meta name="ahrefs-site-verification" content="f5eb09c7a78bc7d73ec49108e2054114ef669fada7cb294f500e603a678ffe40">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="{{ asset('/public/css/layout.css') }}" rel="stylesheet" />
  <meta name="csrf_token" content="{{ csrf_token() }}" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-LQC6EYR85D"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-LQC6EYR85D');
</script>
<script data-ad-client="ca-pub-8088300493188970" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body class="user-body">
  <div class="container">
    <div class="header d-flex justify-content-between align-items-end px-5 py-2 border-bottom">
      <a href="{{env('APP_URL')}}"><img src="{{asset('/public/imgs/logo.png')}}" width="200" height="100" alt=""></a>
      <div class="d-flex" id="search">
        <input type="text" class="form-control rounded-0" value="{{$searchKey}}" />
        <button class="btn btn-success rounded-0">Zoeken</button>
      </div>
    </div>
    <div class="navbar user-menu">
      <ul>
        @foreach($headers as $key => $header)
        <li><a href="{{$header->link}}">{{$header->name}}</a></li>
        @endforeach
      </ul>
    </div>
    @yield('page')

    <div class="footer d-flex justify-content-between">
      @foreach($footers as $key => $footer)
      <div>
        <p>{{$key}}</p>
        <ul>
          @foreach($footer as $child)
          <li><a href="{{$child->link}}">{{$child->name}}</a></li>
          @endforeach
        </ul>
      </div>
      @endforeach
    </div>

  </div>
  </div>
  <script>
    var gSiteURL = "<?php echo env('APP_URL') ?>";
  </script>
  <script src="{{asset('/public/js/user.js')}}"></script>
</body>

</html>